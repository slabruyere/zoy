(defproject zoy "0.1.1-SNAPSHOT"
  :description "A cli to clean up your digital mess."
  :url "https://gitlab.com/slabruyere/zoy/"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url  "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[clj-time "0.15.2"]
                 [com.drewnoakes/metadata-extractor "2.13.0"]
                 [me.raynes/fs "1.4.6"]
                 [org.clojure/clojure "1.9.0"]
                 [org.clojure/tools.cli "1.0.194"]
                 [pandect "0.6.1"]]
  :main ^:skip-aot zoy.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}
             :dev     {:plugins     [[lein-cloverage "1.1.2"]]
                       :global-vars {*warn-on-reflection* true}}})

