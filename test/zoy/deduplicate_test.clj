(ns zoy.deduplicate-test
  (:require [clojure.test :refer [deftest is testing use-fixtures]]
            [zoy.core :refer [zoy]]
            [zoy.logger :refer [get-logger]]
            [zoy.test-utils :refer [src src-backup
                                    get-fake-logger
                                    get-relative-paths-set
                                    restore-initial-state]]))

(use-fixtures :each restore-initial-state)

(deftest do-deduplicate-test
  (testing "Should deduplicate files in src"
    (zoy "deduplicate" src "--keep-one")
    (let [actual-src        (get-relative-paths-set src)
          actual-src-backup (get-relative-paths-set src-backup)
          expected-src      (set (list
                                  "mp4/sample_mpeg4.mp4"
                                  "mp4/uuid540.mp4"
                                  "mov/with-gps.mov"
                                  "mov/xmp480qt.mov"
                                  "pdf/report1.pdf"
                                  "jpg/Sony Ericsson U10i.jpg"
                                  "png/photoshop-8x12-rgb24-all-metadata.png"
                                  "png/sampleWithExifData.png"
                                  "jpg/FujiFilm FinePix A303.jpg"
                                  "jpg/Sony DCR-PC110.jpg"
                                  "pdf/report2.pdf"
                                  "jpg/Apple iPhone 4.jpg"))]
      (is (not= actual-src actual-src-backup))
      (is (= actual-src expected-src)))))

;; partitions won't work with grouping the given files by file-hash... Need an atom.
#_(deftest do-deduplicate-partition-test
    (testing "Should deduplicate with partition files in src"
      (zoy "deduplicate" src "--keep-one" "--partition" "3")
      (let [actual-src        (get-relative-paths-set src)
            actual-src-backup (get-relative-paths-set src-backup)
            expected-src      (set (list
                                    "jpg/test1.jpg"
                                    "jpg/test2.jpeg"
                                    "jpg/test3.JPG"
                                    "jpg/test4.jpg"
                                    "jpg/test5.jpg"
                                    "jpg/test6.jpg"
                                    "mov/test.mov"
                                    "mp4/test.mp4"
                                    "png/test1.png"
                                    "png/test2.png"))]
        (is (not= actual-src actual-src-backup))
        (is (= actual-src expected-src)))))

(deftest do-deduplicate-confirm-yes-test
  (testing "Should deduplicate files in src"
    (with-redefs [read-line (fn [& _] "Y")
                  println   (fn [& _])]
      (zoy "deduplicate" src "--confirm" "--keep-one")
      (let [actual-src        (get-relative-paths-set src)
            actual-src-backup (get-relative-paths-set src-backup)
            expected-src      (set (list
                                    "mp4/sample_mpeg4.mp4"
                                    "mp4/uuid540.mp4"
                                    "mov/with-gps.mov"
                                    "mov/xmp480qt.mov"
                                    "pdf/report1.pdf"
                                    "jpg/Sony Ericsson U10i.jpg"
                                    "png/photoshop-8x12-rgb24-all-metadata.png"
                                    "png/sampleWithExifData.png"
                                    "jpg/FujiFilm FinePix A303.jpg"
                                    "jpg/Sony DCR-PC110.jpg"
                                    "pdf/report2.pdf"
                                    "jpg/Apple iPhone 4.jpg"))]
        (is (not= actual-src actual-src-backup))
        (is (= actual-src expected-src))))))

(deftest do-deduplicate-confirm-no-test
  (testing "Should not deduplicate src since it is a dry-run"
    (let [spy (atom [])]
      (with-redefs [get-logger (fn [options]
                                 (get-fake-logger options spy))
                    read-line  (fn [& _] "Y : won't work because it's not just Y!")
                    println    (fn [& _])]
        (zoy "deduplicate" src "--confirm" "--keep-one" "-v")
        (let [actual-src        (get-relative-paths-set src)
              actual-src-backup (get-relative-paths-set src-backup)]
          (is (= actual-src actual-src-backup))
          (is (= @spy [["info" "Found 11 files."]
                       ["info" "Deleting 1 files."]
                       ["info" "Total reclaimed space: 6.015625kB"]
                       ["info" "No operation will be performed."]
                       ["info" "Ran in 0s"]])))))))

(deftest do-deduplicate-dry-run-test
  (testing "Should not deduplicate src since it is a dry-run"
    (let [spy (atom [])]
      (with-redefs [get-logger (fn [options]
                                 (get-fake-logger options spy))
                    println    (fn [& _])]
        (zoy "deduplicate" src "--keep-one" "-v" "--dry-run")
        (let [actual-src        (get-relative-paths-set src)
              actual-src-backup (get-relative-paths-set src-backup)]
          (is (= actual-src actual-src-backup))
          (is (= @spy [["info" "<dry-run>"]
                       ["info" "Found 11 files."]
                       ["info" "Deleting 1 files."]
                       ["info" "Total reclaimed space: 6.015625kB"]
                       ["info" "</dry-run>"]
                       ["info" "Ran in 0s"]])))))))

(deftest do-deduplicate-rubbish-bin-test
  (testing "Should move to rubbish bin if instructed to do so"
    (let [spy (atom [])]
      (with-redefs [get-logger (fn [options]
                                 (get-fake-logger options spy))
                    println    (fn [& _])]
        (zoy "deduplicate" src "--keep-one" "-v" "--rubbish-bin" "resources/dest/rubbish-bin")
        (let [actual-src        (get-relative-paths-set src)
              actual-src-backup (get-relative-paths-set src-backup)]
          (is (not= actual-src actual-src-backup))
          (is (= @spy [["info" "Found 11 files."]
                       ["info" (str "Creating " (System/getProperty "user.dir") "/resources/dest/rubbish-bin")]
                       ["info" "Moving 1 files."]
                       ["info" "Total reclaimed space: 6.015625kB"]
                       ["info" "Ran in 0s"]])))))))
