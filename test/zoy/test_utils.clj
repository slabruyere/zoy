(ns zoy.test-utils
  (:require [clojure.string :as s]
            [me.raynes.fs :as fs]
            [zoy.utils.files :refer [reset-file-hash-location-map]]))

(def src "resources/src")
(def src-backup "resources/do-not-delete")
(def dest "resources/dest")

(defn restore-initial-state
  "Restore src folder and resets locations atom"
  [f]
  (f)
  (fs/delete-dir src)
  (fs/delete-dir dest)
  (fs/copy-dir src-backup src)
  (reset-file-hash-location-map))

(defn get-fake-logger
  [{:keys [debug verbose]} spy]
  (let [cli-authorized-levels (cond-> #{"error"}
                                verbose (conj "info" "warn")
                                debug   (conj "debug"))]
    (fn [level message]
      (when (cli-authorized-levels level)
        (swap! spy conj [level message])))))

(defn get-relative-path
  [from file]
  (let [from-absolute-path (.getAbsolutePath ^java.io.File (fs/absolute from))
        from-regexp        (re-pattern
                            (str "^"
                                 (s/replace from-absolute-path "/" "\\/")
                                 "\\/(.*)"))
        file-absolute-path (.getAbsolutePath ^java.io.File file)
        matches            (re-matches from-regexp file-absolute-path)]
    (when matches
      (second matches))))

(defn get-relative-paths-set
  [path]
  (->> (fs/find-files path #".*")
       (filter fs/file?)
       (map (partial get-relative-path path))
       (into #{})))

(defn make-set-of-sets-of-two
  [l]
  (->> (partition 2 l)
       (map #(into #{} %))
       (into #{})))
