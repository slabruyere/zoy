(ns zoy.sort-test
  (:require [clojure.test :refer [deftest is testing use-fixtures]]
            [zoy.core :refer [zoy]]
            [zoy.logger :refer [get-logger]]
            [zoy.test-utils :refer [src src-backup dest
                                    get-fake-logger
                                    get-relative-paths-set
                                    restore-initial-state
                                    make-set-of-sets-of-two]]))

(use-fixtures :each restore-initial-state)

(deftest do-sort-test
  (testing "Should sort src and move renamed file to dest"
    (zoy "sort" src dest)
    (let [actual-dest       (get-relative-paths-set dest)
          actual-src        (get-relative-paths-set src)
          actual-src-backup (get-relative-paths-set src-backup)
          expected-dest     (set (list
                                  "2001/04 April/SONY DCRPC100 - 2001-04-01 16:29:03.jpg"
                                  "2003/03 March/FUJIFILM FinePix A303 - 2003-03-04 17:14:51.jpg"
                                  "2005/10 October/Unknown - 2005-10-28 17:46:46.mp4"
                                  "2009/11 November/Sony Ericsson U10i - 2009-11-21 10:19:41.jpg"
                                  "2011/01 January/Apple iPhone 4 - 2011-01-13 14:33:39(1).jpg"
                                  "2011/01 January/Apple iPhone 4 - 2011-01-13 14:33:39.jpg"
                                  "2019/07 July/Apple iPhone 6 - 2019-07-24 08:25:57.mov"
                                  "2020/01 January/Unknown - 2020-01-05 11:19:45.mov"
                                  "2020/01 January/Unknown - 2020-01-05 15:04:00.mp4"
                                  "Unknown/Unknown - Unknown(1).png"
                                  "Unknown/Unknown - Unknown.png"))]
      (is (not= actual-src actual-src-backup))
      (is (= actual-dest expected-dest)))))

(deftest do-sort-confirm-yes-test
  (testing "Should sort src and move renamed file to dest"
    (with-redefs [read-line (fn [& _] "Y")
                  println   (fn [& _])]
      (zoy "sort" src dest "--confirm")
      (let [actual-dest       (get-relative-paths-set dest)
            actual-src        (get-relative-paths-set src)
            actual-src-backup (get-relative-paths-set src-backup)
            expected-dest     (set (list
                                    "2001/04 April/SONY DCRPC100 - 2001-04-01 16:29:03.jpg"
                                    "2003/03 March/FUJIFILM FinePix A303 - 2003-03-04 17:14:51.jpg"
                                    "2005/10 October/Unknown - 2005-10-28 17:46:46.mp4"
                                    "2009/11 November/Sony Ericsson U10i - 2009-11-21 10:19:41.jpg"
                                    "2011/01 January/Apple iPhone 4 - 2011-01-13 14:33:39(1).jpg"
                                    "2011/01 January/Apple iPhone 4 - 2011-01-13 14:33:39.jpg"
                                    "2019/07 July/Apple iPhone 6 - 2019-07-24 08:25:57.mov"
                                    "2020/01 January/Unknown - 2020-01-05 11:19:45.mov"
                                    "2020/01 January/Unknown - 2020-01-05 15:04:00.mp4"
                                    "Unknown/Unknown - Unknown(1).png"
                                    "Unknown/Unknown - Unknown.png"))]
        (is (not= actual-src actual-src-backup))
        (is (= expected-dest actual-dest))))))

(deftest do-sort-partition-test
  (testing "Should sort src with partition and move renamed file to dest"
    (zoy "sort" src dest "--partition" "4")
    (let [actual-dest       (get-relative-paths-set dest)
          actual-src        (get-relative-paths-set src)
          actual-src-backup (get-relative-paths-set src-backup)
          expected-dest     (set (list
                                  "2001/04 April/SONY DCRPC100 - 2001-04-01 16:29:03.jpg"
                                  "2003/03 March/FUJIFILM FinePix A303 - 2003-03-04 17:14:51.jpg"
                                  "2005/10 October/Unknown - 2005-10-28 17:46:46.mp4"
                                  "2009/11 November/Sony Ericsson U10i - 2009-11-21 10:19:41.jpg"
                                  "2011/01 January/Apple iPhone 4 - 2011-01-13 14:33:39(1).jpg"
                                  "2011/01 January/Apple iPhone 4 - 2011-01-13 14:33:39.jpg"
                                  "2019/07 July/Apple iPhone 6 - 2019-07-24 08:25:57.mov"
                                  "2020/01 January/Unknown - 2020-01-05 11:19:45.mov"
                                  "2020/01 January/Unknown - 2020-01-05 15:04:00.mp4"
                                  "Unknown/Unknown - Unknown(1).png"
                                  "Unknown/Unknown - Unknown.png"))]
      (is (not= actual-src actual-src-backup))
      (is (= expected-dest actual-dest)))))

(deftest do-sort-as-selection-test
  (testing "Should sort src and move renamed jpeg file to dest as selection"
    (let [selection "my super folder"]
      (zoy "sort" src dest "--types" "jp(e?)g" "--selection" selection "--as-selection")
      (let [actual-dest       (get-relative-paths-set dest)
            actual-src        (get-relative-paths-set src)
            actual-src-backup (get-relative-paths-set src-backup)
            expected-dest     (set (list
                                    (str "2001/04 April/" selection "/SONY DCRPC100 - 2001-04-01 16:29:03.jpg")
                                    (str "2003/03 March/" selection "/FUJIFILM FinePix A303 - 2003-03-04 17:14:51.jpg")
                                    (str "2009/11 November/" selection "/Sony Ericsson U10i - 2009-11-21 10:19:41.jpg")
                                    (str "2011/01 January/" selection "/Apple iPhone 4 - 2011-01-13 14:33:39(1).jpg")
                                    (str "2011/01 January/" selection "/Apple iPhone 4 - 2011-01-13 14:33:39.jpg")))]
        (is (not= actual-src actual-src-backup))
        (is (= actual-dest expected-dest))))))

(deftest do-sort-dry-run-test
  (testing "Should sort src without renaming files to dest"
    (let [spy (atom [])]
      (with-redefs [get-logger (fn [options]
                                 (get-fake-logger options spy))
                    println    (fn [& _])]
        (zoy "sort" src dest "--verbose" "--dry-run")
        (let [current-working-dir (System/getProperty "user.dir")
              dir-offset          (str current-working-dir "/" dest)
              actual-dest         (get-relative-paths-set dest)
              actual-src          (get-relative-paths-set src)
              actual-src-backup   (get-relative-paths-set src-backup)
              expected-dest       #{}
              expected-logs       [["info" (str "Creating " dir-offset "/2001/04 April")]
                                   ["info" "Moving 1 files"]
                                   ["info" (str "Creating " dir-offset "/2003/03 March")]
                                   ["info" "Moving 1 files"]
                                   ["info" (str "Creating " dir-offset "/2005/10 October")]
                                   ["info" "Moving 1 files"]
                                   ["info" (str "Creating " dir-offset "/2009/11 November")]
                                   ["info" "Moving 1 files"]
                                   ["info" (str "Creating " dir-offset "/2011/01 January")]
                                   ["info" "Moving 2 files"]
                                   ["info" (str "Creating " dir-offset "/2019/07 July")]
                                   ["info" "Moving 1 files"]
                                   ["info" (str "Creating " dir-offset "/2020/01 January")]
                                   ["info" "Moving 2 files"]
                                   ["info" (str "Creating " dir-offset "/Unknown")]
                                   ["info" "Moving 2 files"]]]
          (is (= actual-dest expected-dest))
          (is (= actual-src actual-src-backup))
          (is (= (take 2 @spy) [["info" "<dry-run>"]
                                ["info" "Found 11 files."]]))
          (is (= (make-set-of-sets-of-two (subvec @spy 2 (- (count @spy) 2)))
                 (make-set-of-sets-of-two expected-logs)))
          (is (= (take-last 2 @spy) [["info" "</dry-run>"]
                                     ["info" "Ran in 0s"]])))))))

(deftest do-sort-confirm-no-test
  (testing "Should sort src without renaming files to dest"
    (let [spy (atom [])]
      (with-redefs [read-line  (fn [& _] "Y : won't work because it's not just Y!")
                    get-logger (fn [options]
                                 (get-fake-logger options spy))
                    println    (fn [& _])]
        (zoy "sort" src dest "-v" "--confirm")
        (let [current-working-dir (System/getProperty "user.dir")
              dir-offset          (str current-working-dir "/" dest)
              actual-dest         (get-relative-paths-set dest)
              actual-src          (get-relative-paths-set src)
              actual-src-backup   (get-relative-paths-set src-backup)
              expected-dest       #{}
              expected-logs       [["info" (str "Creating " dir-offset "/2001/04 April")]
                                   ["info" "Moving 1 files"]
                                   ["info" (str "Creating " dir-offset "/2003/03 March")]
                                   ["info" "Moving 1 files"]
                                   ["info" (str "Creating " dir-offset "/2005/10 October")]
                                   ["info" "Moving 1 files"]
                                   ["info" (str "Creating " dir-offset "/2009/11 November")]
                                   ["info" "Moving 1 files"]
                                   ["info" (str "Creating " dir-offset "/2011/01 January")]
                                   ["info" "Moving 2 files"]
                                   ["info" (str "Creating " dir-offset "/2019/07 July")]
                                   ["info" "Moving 1 files"]
                                   ["info" (str "Creating " dir-offset "/2020/01 January")]
                                   ["info" "Moving 2 files"]
                                   ["info" (str "Creating " dir-offset "/Unknown")]
                                   ["info" "Moving 2 files"]]]
          (is (= actual-dest expected-dest))
          (is (= actual-src actual-src-backup))
          (is (= (first @spy) ["info" "Found 11 files."]))
          (is (= (make-set-of-sets-of-two (subvec @spy 1 (- (count @spy) 2)))
                 (make-set-of-sets-of-two expected-logs)))
          (is (= (take-last 2 @spy) [["info" "No operation will be performed."]
                                     ["info" "Ran in 0s"]])))))))

;; Need to find a way to test dry-run partititon properly
;; The challenge is we don't know the exact order of the files being handled so the same directory may appear several times. Grouping by parent folder will not always do the trick either, but would reduce the numer of calls to mkdir...
#_(deftest do-sort-dry-run-partition-test
    (testing "Should sort src without renaming files to dest"
      (let [spy (atom [])]
        (with-redefs [get-logger (fn [options]
                                   (get-fake-logger options spy))
                      println    (fn [& _])]
          (zoy "sort" src dest "--verbose" "--dry-run" "--partition" "6")
          (let [current-working-dir (System/getProperty "user.dir")
                dir-offset          (str current-working-dir "/" dest)
                actual-dest         (get-relative-paths-set dest)
                actual-src          (get-relative-paths-set src)
                actual-src-backup   (get-relative-paths-set src-backup)
                expected-dest       #{}
                expected-logs       [["info" (str "Creating " dir-offset "/1904/01 January")]
                                     ["info" "Moving 1 files"]
                                     ["info" (str "Creating " dir-offset "/2017/06 June")]
                                     ["info" "Moving 1 files"]
                                     ["info" (str "Creating " dir-offset "/2020/02 February")]
                                     ["info" "Moving 1 files"]
                                     ["info" (str "Creating " dir-offset "/2020/04 April")]
                                     ["info" "Moving 1 files"]
                                     ["info" (str "Creating " dir-offset "/2020/01 January")]
                                     ["info" "Moving 1 files"]
                                     ["info" (str "Creating " dir-offset "/2020/05 May")]
                                     ["info" "Moving 1 files"]
                                     ["info" (str "Creating " dir-offset "/Unknown")]
                                     ["info" "Moving 3 files"]
                                     ["info" (str "Creating " dir-offset "/2020/05 May")]
                                     ["info" "Moving 1 files"]
                                     ["info" (str "Creating " dir-offset "/1904/01 January")]
                                     ["info" "Moving 1 files"]]]
            (is (= actual-dest expected-dest))
            (is (= actual-src actual-src-backup))
            (is (= (take 2 @spy) [["info" "<dry-run>"]
                                  ["info" "Found 6 files."]]))
            (is (= (make-set-of-sets-of-two (concat (subvec @spy 2 14)
                                                    (subvec @spy 15 (- (count @spy) 2))))
                   (make-set-of-sets-of-two expected-logs)))
            (is (= (nth @spy 14) ["info" "Found 5 files."]))
            (is (= (take-last 2 @spy) [["info" "</dry-run>"]
                                       ["info" "Ran in 0s"]])))))))

(deftest do-sort-deduplicate-test
  (testing "Should sort src, move renamed file to dest and deduplicate"
    (zoy "sort" src dest "--deduplicate")
    (let [actual-dest       (get-relative-paths-set dest)
          actual-src        (get-relative-paths-set src)
          actual-src-backup (get-relative-paths-set src-backup)
          expected-dest     (set (list
                                  "2001/04 April/SONY DCRPC100 - 2001-04-01 16:29:03.jpg"
                                  "2003/03 March/FUJIFILM FinePix A303 - 2003-03-04 17:14:51.jpg"
                                  "2005/10 October/Unknown - 2005-10-28 17:46:46.mp4"
                                  "2009/11 November/Sony Ericsson U10i - 2009-11-21 10:19:41.jpg"
                                  "2011/01 January/Apple iPhone 4 - 2011-01-13 14:33:39.jpg"
                                  "2019/07 July/Apple iPhone 6 - 2019-07-24 08:25:57.mov"
                                  "2020/01 January/Unknown - 2020-01-05 11:19:45.mov"
                                  "2020/01 January/Unknown - 2020-01-05 15:04:00.mp4"
                                  "Unknown/Unknown - Unknown(1).png"
                                  "Unknown/Unknown - Unknown.png"))]
      (is (not= actual-src actual-src-backup))
      (is (= actual-dest expected-dest)))))

(deftest do-sort-camera-name-test
  (testing "Should sort src, move renamed file to dest and deduplicate"
    (let [camera-name "My Cam"]
      (zoy "sort" src dest "--camera-name" camera-name "--deduplicate")
      (let [actual-dest       (get-relative-paths-set dest)
            actual-src        (get-relative-paths-set src)
            actual-src-backup (get-relative-paths-set src-backup)
            expected-dest     (set (list
                                    (str "2001/04 April/" camera-name " - 2001-04-01 16:29:03.jpg")
                                    (str "2003/03 March/" camera-name " - 2003-03-04 17:14:51.jpg")
                                    (str "2005/10 October/" camera-name " - 2005-10-28 17:46:46.mp4")
                                    (str "2009/11 November/" camera-name " - 2009-11-21 10:19:41.jpg")
                                    (str "2011/01 January/" camera-name " - 2011-01-13 14:33:39.jpg")
                                    (str "2019/07 July/" camera-name " - 2019-07-24 08:25:57.mov")
                                    (str "2020/01 January/" camera-name " - 2020-01-05 11:19:45.mov")
                                    (str "2020/01 January/" camera-name " - 2020-01-05 15:04:00.mp4")
                                    (str "Unknown/" camera-name " - Unknown(1).png")
                                    (str "Unknown/" camera-name " - Unknown.png")))]
        (is (not= actual-src actual-src-backup))
        (is (= actual-dest expected-dest))))))
