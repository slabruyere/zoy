(ns zoy.core-test
  (:require [clojure.test :refer [deftest is testing use-fixtures]]
            [zoy.core :refer [zoy exit]]
            [zoy.utils.cli :refer [get-version]]))

(def ^:private message (atom nil))

(def ^:private status (atom nil))

(use-fixtures :each (fn [f]
                      (f)
                      (reset! message nil)
                      (reset! status nil)))

(deftest zoy-help-test
  (testing "Should return zoy help"
    (with-redefs [exit (fn [s m]
                         (reset! status s)
                         (reset! message m))]
      (zoy "--help")
      (is (.contains ^java.lang.String @message "Usage: zoy COMMAND ARGUMENTS [OPTIONS]"))
      (is (zero? @status)))))

(deftest zoy-something-test
  (testing "Should return zoy help"
    (with-redefs [exit (fn [s m]
                         (reset! status s)
                         (reset! message m))]
      (zoy "something" "somtheing" "something")
      (is (.contains ^java.lang.String @message "Usage: zoy COMMAND ARGUMENTS [OPTIONS]"))
      (is (= 1 @status)))))

(deftest zoy-version-test
  (testing "Should return zoy version"
    (with-redefs [exit (fn [s m]
                         (reset! status s)
                         (reset! message m))]
      (zoy "--version")
      (is (= (get-version) @message))
      (is (zero? @status)))))

(deftest zoy-something-version-test
  (testing "Should return zoy version"
    (with-redefs [exit (fn [s m]
                         (reset! status s)
                         (reset! message m))]
      (zoy "something" "--version" "something")
      (is (= (get-version) @message))
      (is (zero? @status)))))

(deftest zoy-something-legit-version-test
  (testing "Should return zoy version"
    (with-redefs [exit (fn [s m]
                         (reset! status s)
                         (reset! message m))]
      (zoy "deduplicate" "resources/src" "--version")
      (is (= (get-version) @message))
      (is (zero? @status)))))

(deftest zoy-deduplicate-help-test
  (testing "Should return zoy deduplicate help"
    (with-redefs [exit (fn [s m]
                         (reset! status s)
                         (reset! message m))]
      (zoy "deduplicate" "--help")
      (is (.contains ^java.lang.String @message "Usage: zoy deduplicate SRC [OPTIONS]"))
      (is (zero? @status)))))

(deftest zoy-deduplicate-something-help-test
  (testing "Should return zoy deduplicate help"
    (with-redefs [exit (fn [s m]
                         (reset! status s)
                         (reset! message m))]
      (zoy "deduplicate" "resources/src" "-v" "--dry-run" "--help")
      (is (.contains ^java.lang.String @message "Usage: zoy deduplicate SRC [OPTIONS]"))
      (is (zero? @status)))))

(deftest zoy-rename-help-test
  (testing "Should return zoy rename help"
    (with-redefs [exit (fn [s m]
                         (reset! status s)
                         (reset! message m))]
      (zoy "rename" "--help")
      (is (.contains ^java.lang.String @message "Usage: zoy rename SRC [OPTIONS]"))
      (is (zero? @status)))))

(deftest zoy-rename-something-help-test
  (testing "Should return zoy help"
    (with-redefs [exit (fn [s m]
                         (reset! status s)
                         (reset! message m))]
      (zoy "rename" "resources/src" "-v" "--dry-run" "--help")
      (is (.contains ^java.lang.String @message "Usage: zoy rename SRC [OPTIONS]"))
      (is (zero? @status)))))

(deftest zoy-sort-help-test
  (testing "Should return zoy sort help"
    (with-redefs [exit (fn [s m]
                         (reset! status s)
                         (reset! message m))]
      (zoy "sort" "--help")
      (is (.contains ^java.lang.String @message "Usage: zoy sort SRC DEST [OPTIONS]"))
      (is (zero? @status)))))

(deftest zoy-sort-something-help-test
  (testing "Should return zoy help"
    (with-redefs [exit (fn [s m]
                         (reset! status s)
                         (reset! message m))]
      (zoy "sort" "resources/src" "resources/dest" "-v" "--dry-run" "--help")
      (is (.contains ^java.lang.String @message "Usage: zoy sort SRC DEST [OPTIONS]"))
      (is (zero? @status)))))
