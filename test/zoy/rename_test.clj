(ns zoy.rename-test
  (:require [clojure.test :refer [deftest is testing use-fixtures]]
            [zoy.core :refer [zoy]]
            [zoy.logger :refer [get-logger]]
            [zoy.test-utils :refer [src src-backup
                                    get-fake-logger
                                    get-relative-paths-set
                                    restore-initial-state]]))

(use-fixtures :each restore-initial-state)

(deftest do-rename-test
  (testing "Should rename files in src but not move them"
    (zoy "rename" src)
    (let [actual-src        (get-relative-paths-set src)
          actual-src-backup (get-relative-paths-set src-backup)
          expected-src      (set (list
                                  "jpg/SONY DCRPC100 - 2001-04-01 16:29:03.jpg"
                                  "jpg/Apple iPhone 4 - 2011-01-13 14:33:39.jpg"
                                  "mov/Apple iPhone 6 - 2019-07-24 08:25:57.mov"
                                  "mp4/Unknown - 2005-10-28 17:46:46.mp4"
                                  "pdf/report1.pdf"
                                  "jpg/FUJIFILM FinePix A303 - 2003-03-04 17:14:51.jpg"
                                  "png/Unknown - Unknown(1).png"
                                  "png/Unknown - Unknown.png"
                                  "jpg/Apple iPhone 4 - 2011-01-13 14:33:39(1).jpg"
                                  "mov/Unknown - 2020-01-05 11:19:45.mov"
                                  "pdf/report2.pdf"
                                  "jpg/Sony Ericsson U10i - 2009-11-21 10:19:41.jpg"
                                  "mp4/Unknown - 2020-01-05 15:04:00.mp4"))]
      (is (not= actual-src actual-src-backup))
      (is (= actual-src expected-src)))))

(deftest do-rename-partition-test
  (testing "Should rename batches of files in src but not move them"
    (zoy "rename" src "--partition" "100")
    (let [actual-src        (get-relative-paths-set src)
          actual-src-backup (get-relative-paths-set src-backup)
          expected-src      (set (list
                                  "jpg/SONY DCRPC100 - 2001-04-01 16:29:03.jpg"
                                  "jpg/Apple iPhone 4 - 2011-01-13 14:33:39.jpg"
                                  "mov/Apple iPhone 6 - 2019-07-24 08:25:57.mov"
                                  "mp4/Unknown - 2005-10-28 17:46:46.mp4"
                                  "pdf/report1.pdf"
                                  "jpg/FUJIFILM FinePix A303 - 2003-03-04 17:14:51.jpg"
                                  "png/Unknown - Unknown(1).png"
                                  "png/Unknown - Unknown.png"
                                  "jpg/Apple iPhone 4 - 2011-01-13 14:33:39(1).jpg"
                                  "mov/Unknown - 2020-01-05 11:19:45.mov"
                                  "pdf/report2.pdf"
                                  "jpg/Sony Ericsson U10i - 2009-11-21 10:19:41.jpg"
                                  "mp4/Unknown - 2020-01-05 15:04:00.mp4"))]
      (is (not= actual-src actual-src-backup))
      (is (= actual-src expected-src)))))

(deftest do-rename-confirm-yes-test
  (testing "Should rename files in src but not move them"
    (with-redefs [read-line (fn [& _] "Y")
                  println   (fn [& _])]
      (zoy "rename" src "--confirm")
      (let [actual-src        (get-relative-paths-set src)
            actual-src-backup (get-relative-paths-set src-backup)
            expected-src      (set (list
                                    "jpg/SONY DCRPC100 - 2001-04-01 16:29:03.jpg"
                                    "jpg/Apple iPhone 4 - 2011-01-13 14:33:39.jpg"
                                    "mov/Apple iPhone 6 - 2019-07-24 08:25:57.mov"
                                    "mp4/Unknown - 2005-10-28 17:46:46.mp4"
                                    "pdf/report1.pdf"
                                    "jpg/FUJIFILM FinePix A303 - 2003-03-04 17:14:51.jpg"
                                    "png/Unknown - Unknown(1).png"
                                    "png/Unknown - Unknown.png"
                                    "jpg/Apple iPhone 4 - 2011-01-13 14:33:39(1).jpg"
                                    "mov/Unknown - 2020-01-05 11:19:45.mov"
                                    "pdf/report2.pdf"
                                    "jpg/Sony Ericsson U10i - 2009-11-21 10:19:41.jpg"
                                    "mp4/Unknown - 2020-01-05 15:04:00.mp4"))]
        (is (not= actual-src actual-src-backup))
        (is (= actual-src expected-src))))))

(deftest do-rename-dry-run-test
  (testing "Should not rename files in src since it is a dry-run"
    (let [spy (atom [])]
      (with-redefs [get-logger (fn [options]
                                 (get-fake-logger options spy))]
        (zoy "rename" src "-v" "--dry-run")
        (let [actual-src        (get-relative-paths-set src)
              actual-src-backup (get-relative-paths-set src-backup)]
          (is (= actual-src actual-src-backup))
          (is (= @spy [["info" "<dry-run>"]
                       ["info" "Found 11 files."]
                       ["info" "Renaming 11 files."]
                       ["info" "</dry-run>"]
                       ["info" "Ran in 0s"]])))))))

(deftest do-rename-confirm-no-test
  (testing "Should not rename files in src since it is a dry-run"
    (let [spy (atom [])]
      (with-redefs [get-logger (fn [options]
                                 (get-fake-logger options spy))
                    read-line  (fn [& _] "Y : won't work because it's not just Y!")
                    println    (fn [& _])]
        (zoy "rename" src "-v" "--confirm")
        (let [actual-src        (get-relative-paths-set src)
              actual-src-backup (get-relative-paths-set src-backup)]
          (is (= actual-src actual-src-backup))
          (is (= @spy [["info" "Found 11 files."]
                       ["info" "Renaming 11 files."]
                       ["info" "No operation will be performed."]
                       ["info" "Ran in 0s"]])))))))
