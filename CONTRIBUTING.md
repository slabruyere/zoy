# Developing `zoy`

You need [lein](https://leiningen.org/) for running JVM tests and/or producing uberjars.

## Clone repository

```shellsession
$ git clone https://gitlab.com/slabruyere/zoy.git
```

## REPL
You can use zoy directly in the REPL by requiring it from `zoy.core`. Argum
```shellsession
$ lein repl
$ (require '[zoy.core :refer [zoy]])
$ (zoy "sort" "resources/src" "resources/dest" "--dry-run" "--verbose")
```

## Test
To run the tests, make sure the `resources` folder has been restored by running:
```shellsession
$ ./scripts/restore_resources.sh
$ lein run
```

## Lint and check test coverage

To make sure your build passes, run [clj-kondo](https://github.com/borkdude/clj-kondo) and [cloverage](https://github.com/cloverage/cloverage):
```shellsession
$ lein cloverage --no-html --fail-threshold 85
$ clj-kondo --lint src test --config '{:output {:exclude-files ["java"]}}'
```

## Issues
Feel free to open issues [here](https://gitlab.com/slabruyere/zoy/-/issues/new) if needed.
