#!/usr/bin/env bash

if [[ -z "$1" ]]; then
  echo "Usage: ./bump_version.sh [major|minor|patch] [--dry-run]"
  exit 1
fi

# Get zoy current version
zoy_wip_version=$(cat ZOY_VERSION)
zoy_version=${zoy_wip_version%-SNAPSHOT}

if [[ "$2" == "--dry-run" ]]; then
  echo "zoy_wip_version:" $zoy_wip_version
  echo "zoy_version:" $zoy_version
fi

# Get major, minor and patch
major=$(echo $zoy_version | cut -d. -f1)
minor=$(echo $zoy_version | cut -d. -f2)
patch=$(echo $zoy_version | cut -d. -f3)

if [[ "$2" == "--dry-run" ]]; then
  echo "major:" $major
  echo "minor:" $minor
  echo "patch:" $patch
fi

if [[ "$1" == "major" ]]; then
  major=$((major+1))
  minor=0
  patch=0
elif [[ "$1" == "minor" ]]; then
  minor=$((minor+1))
  patch=0
fi

# Update released and WIP version
new_released_version=$(echo $major.$minor.$patch)
new_wip_version=$(echo $major.$minor.$((patch+1))-SNAPSHOT)

if [[ "$2" == "--dry-run" ]]; then
  echo "new_wip_version:" $new_wip_version
  echo "new_released_version:" $new_released_version
  sed -re '1 s/'"$zoy_wip_version"'/'"$new_wip_version"'/' project.clj
  sed -re 's/'"$zoy_wip_version"'/'"$new_wip_version"'/g' README.md

else
  # Update project.clj
  sed -re '1 s/'"$zoy_wip_version"'/'"$new_wip_version"'/' project.clj -i
  sed -re 's/'"$zoy_wip_version"'/'"$new_wip_version"'/g' README.md -i

  # Update ZOY_VERSION
  echo $new_wip_version > ZOY_VERSION

  # Update ZOY_RELEASED_VERSION
  echo $new_released_version > ZOY_RELEASED_VERSION

  # Create git commit and tag
  git commit -am "version $new_released_version"
  git tag -a "$new_released_version" -m "version $new_released_version"
fi
