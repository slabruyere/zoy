#!/usr/bin/env bash

# delete everything
rm -rf resources/src resources/dest resources/my-reports resources/my-trips resources/my-pictures

# create the necessary folders
mkdir -p resources/my-pictures/a-folder resources/my-pictures/another-folder resources/my-reports/a-folder/another-folder resources/my-trips/Australia resources/my-trips/France

# copy do-not-delete into src
cp -R resources/do-not-delete resources/src

# organize my-reports
cp resources/do-not-delete/pdf/report1.pdf resources/my-reports/report1.pdf
cp resources/do-not-delete/pdf/report1.pdf resources/my-reports/a-folder/report1-copy.pdf
cp resources/do-not-delete/pdf/report1.pdf resources/my-reports/a-folder/another-folder/report1-another-copy.pdf
cp resources/do-not-delete/pdf/report2.pdf resources/my-reports/report2.pdf
cp resources/do-not-delete/pdf/report2.pdf resources/my-reports/a-folder/another-folder/report2-copy.pdf
cp resources/do-not-delete/jpg/Apple\ iPhone\ 4.jpg resources/my-reports/an-image.jpg

# organize my-trips
cp resources/do-not-delete/jpg/Apple\ iPhone\ 4.jpg resources/my-trips/Australia/australia.jpg
cp resources/do-not-delete/mp4/sample_mpeg4.mp4 resources/my-trips/Australia/australia.mp4
cp resources/do-not-delete/jpg/FujiFilm\ FinePix\ A303.jpg resources/my-trips/France/france.jpg
cp resources/do-not-delete/mov/with-gps.mov resources/my-trips/France/france.mov

# organize my-pictures
cp resources/do-not-delete/jpg/Apple\ iPhone\ 4.jpg resources/my-pictures/picture1.jpg
cp resources/do-not-delete/jpg/FujiFilm\ FinePix\ A303.jpg resources/my-pictures/picture2.jpg
cp resources/do-not-delete/jpg/Sony\ DCR-PC110.jpg resources/my-pictures/a-folder/picture3.jpg
cp resources/do-not-delete/png/photoshop-8x12-rgb24-all-metadata.png resources/my-pictures/a-folder/picture4.png
cp resources/do-not-delete/jpg/Sony\ DCR-PC110.jpg resources/my-pictures/another-folder/picture3-copy.jpg
cp resources/do-not-delete/png/sampleWithExifData.png resources/my-pictures/another-folder/picture5.png
