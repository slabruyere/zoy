#!/usr/bin/env bash

set -euo pipefail

default_install_dir="/usr/local/bin"
install_dir=$default_install_dir
install_dir_opt=${1:-}
if [ "$install_dir_opt" ]; then
    install_dir="$install_dir_opt"
fi

download_dir=/tmp

latest_release="$(curl -sL https://gitlab.com/slabruyere/zoy/-/raw/master/ZOY_RELEASED_VERSION)"

download_url="https://gitlab.com/slabruyere/zoy/-/jobs/artifacts/$latest_release/download?job=build-deploy"

cd "$download_dir"
echo -e "Downloading $download_url."
curl -o "zoy-$latest_release.zip" -sL $download_url
unzip -qqo "zoy-$latest_release.zip"
rm "zoy-$latest_release.zip"

mv -f "$download_dir/zoy" "$install_dir/zoy"

echo "Successfully installed zoy in $install_dir."
