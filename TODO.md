# Todo

## Nice-to-have
- [ ] Create release when deploying
- [ ] Automate usage section in README.md
- [ ] Make deduplicate --partition idempotent
- [ ] `--date` to enforce date?
- [ ] `--clean` option to delete empty directories after sorting / deduplicating
- [ ] Increase test coverage

## v1
- [ ] Create "albums" (relative of absolute paths)
- [ ] Use GraalVM compiler to make this natively binary
- [ ] `--camera-names CAMERA-NAMES-CSV` to allow renaming of cameras
- [ ] `--get-cameras` to get camera names to replace
- [ ] `deduplicate --hard` to compare pixel by pixel rather than file hash
- [ ] `deduplicate --deep` to have algorithm find compressed versions of a picture
