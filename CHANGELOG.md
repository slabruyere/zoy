# Change Log
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

## [0.1.0] - 2020-05-26
### Added
- `zoy` cli with 3 commands: deduplicate, rename, sort.
- tests with a coverage of 86%
- documentation in README.md with examples
