(ns zoy.core
  (:require [clj-time.core :as t]
            [clojure.string :as s]
            [clojure.tools.cli :refer [parse-opts]]
            [me.raynes.fs :as fs]
            [zoy.deduplicate :refer [do-deduplicate]]
            [zoy.logger :refer [get-logger]]
            [zoy.operations :refer [perform-operation]]
            [zoy.sort-rename :refer [do-sort do-rename]]
            [zoy.utils.cli :refer [error-msg
                                   get-version
                                   validate-arguments
                                   get-commands-summary]]
            [zoy.utils.files :refer [make-absolute-path
                                     find-files]])
  (:gen-class))

(def ^:private global-cli-options
  (->> [ ;; options with shortcuts
        ["-o" "--out FILENAME" "Filename for logs to be written in."]
        ["-v" "--verbose" "Print logs."]
        ["-t" "--types TYPES" "Comma-separated file types."
         :default ["jp(e?)g" "png" "mp4" "mov"]
         :parse-fn #(remove empty? (s/split % #","))
         :validate [#(every? (fn [t] (try (re-pattern t)
                                          (catch Exception _
                                            false))) %)
                    "Should be a valid filetype."]]

        ;; long-form only options
        [nil "--confirm" "Run a dry-run, then ask for user confirmation to perform operations."]
        [nil "--dry-run" "Perform a command dry run."]
        [nil "--debug" "Show debug logs."]
        [nil "--partition PARTITION" "Partition files to handle into lists of size given by this option."
         :parse-fn #(Integer/parseInt %)
         :validate [pos-int? "Partition option must be an integer and greater than zero."]]

        ;; help and version
        [nil "--version" "Print the current version number of zoy."]
        ["-h" "--help" "Print this help text."]]
       (sort-by second)))

(def ^:private commands
  {"deduplicate" {:description "Delete duplicate files, using sha1 hash comparison."
                  :details     "This command checks the hashes of files of desired type in the given source directory and keeps only one copy."
                  :arguments   [["src" "Source folder for deduplication."
                                 :parse-fn make-absolute-path
                                 :validate [fs/absolute? "Source folder should be absolute."
                                            fs/directory? "Source folder should be a directory."]]]
                  :options     [[nil "--keep-one" "Keep only one file when deduplicating."]
                                [nil "--selection SELECTION" "Name of selection folder."
                                 :default "Selection"]
                                [nil "--rubbish-bin RUBBISH-BIN" "Destination for deleted-to-be files."
                                 :parse-fn make-absolute-path
                                 :validate [fs/absolute? "Rubbish bin should be absolute."
                                            #(or (fs/directory? %) (not (fs/exists? %))) "Rubbish bin should eiter be a directory or not exist."]]]}

   "rename" {:description "Rename files but keep them in the same folder."
             :details     "This command uses file metadata for pictures and videos in given source directory and renames them accordingly (pattern: `camera - year-month-day hour:minute:seconds.extension`)."
             :arguments   [["src" "Source folder for renaming."
                            :parse-fn make-absolute-path
                            :validate [fs/absolute? "Source folder should be absolute."
                                       fs/directory? "Source folder should be a directory."]]]
             :options     [[nil "--camera-name CAMERA_NAME" "Value for camera name."]
                           [nil "--unknown UNKNOWN" "Value for unknown dates, makes and models."
                            :default "Unknown"]]}

   "sort" {:description "Rename and move files in organized folders."
           :details     "This command renames the files from source directory and moves them to the destination directory in the `year/month` folder."
           :arguments   [["src" "Source folder for sorting."
                          :parse-fn make-absolute-path
                          :validate [fs/absolute? "Source folder should be absolute."
                                     fs/directory? "Source folder should be a directory."]]
                         ["dest" "Destination folder for sorted files."
                          :parse-fn make-absolute-path
                          :validate [fs/absolute? "Destination folder should be absolute."
                                     #(or (fs/directory? %) (not (fs/exists? %))) "Destination folder should eiter be a directory or not exist."]]]
           :options     [[nil "--as-selection" "Consider all the sorted files as selection."]
                         [nil "--camera-name CAMERA_NAME" "Value for camera name."]
                         [nil "--deduplicate" "Delete duplicates too."]
                         [nil "--selection SELECTION" "Name of selection folder."
                          :default "Selection"]
                         [nil "--unknown UNKNOWN" "Value for unknown dates, makes and models."
                          :default "Unknown"]]}})

(def ^:private possible-commands
  (into #{} (keys commands)))

(def ^:private all-cli-options
  (->> commands
       (mapcat (comp :options val))
       (concat global-cli-options)
       (into #{})
       (into [])))

(defn ^:private get-usage
  "Builds the usage summary using global-cli-options and commands definition."
  [global-options-summary]
  (->> [""
        "Usage: zoy COMMAND ARGUMENTS [OPTIONS]"
        ""
        "A cli to clean up your digital mess."
        ""
        "Gloabl options:"
        global-options-summary
        ""
        "Commands:"
        ""
        (get-commands-summary commands)
        ""
        "Type \"zoy COMMAND --help\" for more information on a specific command."
        ""]
       (s/join \newline)))

(defn ^:private validate-possible-command
  [command arguments options]
  (let [{global-options-summary :summary} (parse-opts [] global-cli-options)]
    (validate-arguments command (rest arguments) options commands global-options-summary)))

(defn ^:private validate-args
  "Validate command line arguments. Either return a map indicating the program
  should exit (with a error message, and optional ok status), or a map
  indicating the command the program should take and the options provided."
  [args]
  (let [{arguments :arguments}           (parse-opts args all-cli-options)
        command                          (first arguments)
        {:keys [options errors summary]} (->> (get-in commands [command :options] [])
                                              (concat global-cli-options)
                                              (parse-opts args))]
    (cond
      (and (nil? command)
           (:help options)) {:exit-message (get-usage summary)
                             :ok?          true}

      (:version options) {:exit-message (get-version)
                          :ok?          true}

      errors {:exit-message (error-msg :options errors)}

      (possible-commands command) (validate-possible-command command arguments options)

      :else {:exit-message (get-usage summary)})))

(defn exit
  "Exit program with given status after printing given message."
  [status msg]
  (println msg)
  (System/exit status))

(defn ^:private perform-operations
  [options logger operations]
  (doseq [operation operations]
    (perform-operation options logger operation)))

(defn ^:private ask-confirmation
  [options logger operations]
  (let [tmp-options (assoc options
                           :dry-run true
                           :verbose true)
        tmp-logger  (get-logger tmp-options)]
    (perform-operations tmp-options tmp-logger operations))
  (println "\nDo you want to continue and perform those operations? [Y/n]")
  (if (= "Y" (read-line))
    (perform-operations options logger operations)
    (logger "info" "No operation will be performed.")))

(defn ^:private spark-zoy
  [command {src :src :as arguments} {:keys [partition rubbish-bin] :as options} logger]
  (when (:dry-run options)
    (logger "info" "<dry-run>"))
  (when (and (:confirm options) (:dry-run options))
    (logger "warn" "--confirm option ignored for dry-run"))
  (let [files             (find-files src (:types options))
        do-command        (case command
                            "deduplicate" (partial do-deduplicate arguments options logger)
                            "rename"      (partial do-rename arguments options logger)
                            "sort"        (partial do-sort arguments options logger))
        partitioned-files (if partition
                            (partition-all partition files)
                            [files])]
    (doseq [[idx files-partition] (map list (range (count partitioned-files)) partitioned-files)]
      (let [raw-operations (do-command files-partition)
            operations     (cond->> raw-operations
                             (and (zero? idx)
                                  rubbish-bin) (cons {:action :mkdir
                                                      :args   [rubbish-bin]}))]
        (if (and (:confirm options) (not (:dry-run options)))
          (ask-confirmation options logger operations)
          (perform-operations options logger operations)))))
  (when (:dry-run options)
    (logger "info" "</dry-run>")))

(defn zoy
  "A cli to clean up your digital mess."
  [& args]
  (let [{:keys [command arguments options exit-message ok?] :as cmd} (validate-args args)
        logger                                                       (get-logger options)
        ts1                                                          (t/now)]
    (logger "debug" (str "Command: " cmd))
    (if exit-message
      (exit (if ok? 0 1) exit-message)
      (spark-zoy command arguments options logger))
    (let [ts2          (t/now)
          elapsed-time (t/in-seconds (t/interval ts1 ts2))]
      (logger "info" (str "Ran in " elapsed-time "s")))))

(def -main zoy)
