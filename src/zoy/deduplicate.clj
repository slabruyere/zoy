(ns zoy.deduplicate
  (:require [clojure.string :refer [join]]
            [zoy.utils.size :refer [get-reclaimed-space]]
            [zoy.utils.operations :refer [create-rm-operation]]
            [zoy.utils.information :refer [get-information-summary]]))

(defn ^:private group-contains-more-than-one?
  [[_ group]]
  (> (count group) 1))

(defn ^:private is-copy-or-in-mess-folder?
  [row]
  (or (re-find #"(?i)/mess/" (:absolute_path row))
      (re-find #"(?i)(copy|\(\d+\))" (:name row))))

(defn ^:private compare-duplicates
  "Compare files with the same hash.
  If files are in the same folder, the one with the shorter name will be kept, to allow idempotency with sort command.
  If one file is in the selection subfolder of the same parent, it will be kept.
  Finally, if one file is in a 'mess' folder, it won't be prioritized."
  [{selection :selection} file1 file2]
  (if (= (:parent_folder file1) (:parent_folder file2))
    (compare (count (:name file1)) (count (:name file2)))
    (if (or (= (:parent_folder file1) (str (:parent_folder file2) "/" selection))
            (= (str (:parent_folder file1) "/" selection) (:parent_folder file2)))
      (compare (count (:absolute_path file2)) (count (:absolute_path file1)))
      (compare (is-copy-or-in-mess-folder? file1) (is-copy-or-in-mess-folder? file2)))))

(defn ^:private keep-and-delete
  "Establish keep and delete lists."
  [options [hash rows]]
  (if (:keep-one options)
    (let [sorted-files (sort (partial compare-duplicates options) rows)]
      [hash {:keep   (first sorted-files)
             :delete (rest sorted-files)}])
    (let [items-to-delete (filter is-copy-or-in-mess-folder? rows)
          items-to-keep   (remove is-copy-or-in-mess-folder? rows)
          delete-list     (if (= (count rows) (count items-to-delete))
                            (rest items-to-delete)
                            items-to-delete)
          keep-list       (if (= (count rows) (count items-to-delete))
                            (first items-to-delete)
                            items-to-keep)]
      [hash {:keep   keep-list
             :delete delete-list}])))

(defn ^:private find-duplicates
  "Group files by hash and keep only groups of at least 2 files."
  [files]
  (->> (map get-information-summary files)
       (group-by :hash)
       (filter group-contains-more-than-one?)))

(defn do-deduplicate
  "Return the deduplicate operations."
  [_ options logger files]
  (try
    (logger "info" (str "Found " (count files) " files."))
    (let [duplicates   (find-duplicates files)
          sorted-files (map (partial keep-and-delete options) duplicates)
          to-delete    (mapcat (comp :delete second) sorted-files)
          deleting     (if (:rubbish-bin options)
                         "Moving"
                         "Deleting")]
      (concat [{:action :log
                :args   ["debug" (join \newline duplicates)]}
               {:action :log
                :args   ["info" (str deleting " " (count to-delete) " files.")]}
               {:action :log
                :args   ["debug" (join \newline to-delete)]}]
              (map create-rm-operation to-delete)
              [{:action :log
                :args   ["info" (str "Total reclaimed space: " (get-reclaimed-space to-delete))]}]))
    (catch Exception e
      (logger "error" (.getMessage e)))))
