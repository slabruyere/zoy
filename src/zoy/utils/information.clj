(ns zoy.utils.information
  (:require [clj-time.format :as f]
            [clojure.string :as s]
            [me.raynes.fs :as fs]
            [pandect.algo.sha1 :refer [sha1-file]]
            [zoy.utils.parse :refer [parse-int]])
  (:import [com.drew.imaging ImageMetadataReader]
           [java.util Locale]))

(defn ^:private parse-dimension
  [dimension]
  (try
    (let [matches (re-find #"(\d*)\spixels" dimension)]
      (when (> (count matches) 1)
        (parse-int (second matches))))
    (catch Exception _
      #_(println "Parse dimension" dimension "failed."))))

(defn ^:private find-date
  "TODO: write something smarter than nested try/catch
  This should probably start with a pipeline of fallbacks for the date value.
  And go on with a pipeline of regex matching to determine best formatter."
  [information]
  (try
    (let [date (or (get information "Create Date")
                   (get information "Date/Time Original")
                   (get information "Date/Time")
                   (get information "Date/Time Digitized"))]
      (if date
        (f/parse (f/formatter "YYYY:MM:dd HH:mm:ss") date)
        (let [date (get information "Creation Time")]
          (try
            (f/parse (f/with-locale (f/formatter "EEE. MMM. dd HH:mm:ss Z YYYY") Locale/US) date)
            (catch Exception _
              ;; fix gitlab CI dot removal weirdness
              (f/parse (f/with-locale (f/formatter "EEE MMM dd HH:mm:ss Z YYYY") Locale/US) date))))))
    (catch Exception _
      #_(println "Find date failed for information:\n" information))))

(defn ^:private is-timezone?
  [tz]
  (when tz
    (re-find #"(\+|-)\d{2}:\d{2}" tz)))

(defn ^:private find-timezone
  [information]
  (ffirst
   (filter
    identity
    (map
     is-timezone?
     (vals information)))))

(defn ^:private extract-from-tag
  [tag]
  (into {} (map #(hash-map (.getTagName ^com.drew.metadata.Tag %)
                           (.getDescription ^com.drew.metadata.Tag %))
                tag)))

(defn ^:private information-for-file
  "Takes an image file (as a java.io.InputStream or java.io.File) and extracts exif information into a map"
  [file]
  (try
    (let [metadata    (ImageMetadataReader/readMetadata ^java.io.File file)
          directories (.getDirectories ^com.drew.metadata.Metadata metadata)
          tags        (map #(.getTags ^com.drew.metadata.Directory %) directories)]
      (into {} (map extract-from-tag tags)))
    (catch Exception _
      (println "Could not read metadata for:" (.getAbsolutePath ^java.io.File file))
      {})))

(defn get-information-summary
  [file]
  {:absolute_path (.getAbsolutePath ^java.io.File file)
   :parent_folder (.getParent ^java.io.File file)
   :name          (.getName ^java.io.File file)
   :size          (fs/size file)
   :hash          (sha1-file file)})

(defn get-full-information
  [file]
  (let [information   (information-for-file file)
        absolute-path (.getAbsolutePath ^java.io.File file)]
    {:absolute_path absolute-path
     :parent_folder (.getParent ^java.io.File file)
     :extension     (-> file fs/extension s/lower-case)
     :hash          (sha1-file file)
     :orientation   (get information "Orientation")
     :make          (get information "Make")
     :model         (get information "Model")
     :width         (parse-dimension (or (get information "Width")
                                         (get information "Image Width")))
     :height        (parse-dimension (or (get information "Height")
                                         (get information "Image Height")))
     :date          (find-date information)
     :timezone      (find-timezone information)
     :size          (fs/size file)
     :favourite     (not
                     (nil?
                      (re-find #"(?i)(favourite|best|selection|imprimer)"
                               absolute-path)))}))

(defn ^:private clean-make-model
  [make-model]
  (some-> make-model
          (s/replace #"[^\w\s]+" "")
          (s/replace #"\s+" " ")))

(defn get-camera-name
  [make model]
  (let [camera-name (s/trim
                     (str (clean-make-model make) " "
                          (clean-make-model model)))]
    (when-not (empty? camera-name)
      camera-name)))
