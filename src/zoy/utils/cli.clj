(ns zoy.utils.cli
  (:require [clojure.string :as s]
            [clojure.tools.cli :refer [parse-opts]]))

(defn get-version
  []
  (let [version (-> "project.clj" slurp read-string (nth 2))]
    (str "zoy version " version)))

(defn  error-msg
  [type errors]
  (let [errs (if (= type :arguments)
               (map #(s/replace % #"\"--(\w+)\s(.*)\"" "\"<$1> $2\"") errors)
               errors)]
    (str "\nThe following errors occurred while parsing your command:\n\n"
         (s/join \newline errs)
         \newline)))

(defn ^:private normalize-argument-options
  [[arg-name description & options]]
  (concat
   [nil (str "--" arg-name " " (s/upper-case arg-name)) description]
   options))

(defn ^:private normalize-commands
  [commands]
  (->> commands
       (map (juxt key
                  (comp #(assoc % :arguments
                                (map normalize-argument-options (:arguments %)))
                        val)))
       (into {})))

(defn ^:private validate-arity
  [command arguments commands]
  (let [n-actual   (count arguments)
        n-expected (count (get-in commands [command :arguments]))]
    (when-not (= n-actual n-expected)
      (str "Command \"" command "\" expects exactly " n-expected " arguments, but was given " n-actual ": " (s/join ", " arguments) " )."))))

(defn ^:private get-arguments-summary
  "Build arguments summary."
  [arguments]
  (let [spacer "  "
        mx     (apply max
                      (map (comp count first) arguments))]
    (->> (map (fn [a]
                (str spacer
                     (s/upper-case (first a))
                     (apply str (repeat (- mx (count (first a))) " "))
                     spacer
                     (second a)))
              arguments)
         (s/join \newline))))

(defn ^:private get-command-usage
  "Build usage string for a given command."
  [command commands global-options-summary]
  (let [command-arguments        (get-in commands [command :arguments])
        args                     (->> command-arguments
                                      (map
                                       (fn [st]
                                         (s/upper-case (first st))))
                                      (s/join " "))
        arguments-summary        (get-arguments-summary command-arguments)
        specific-options-summary (:summary (parse-opts [] (get-in commands [command :options])))        ]
    (->> [""
          (str "Usage: zoy " command " " args " [OPTIONS]")
          ""
          (get-in commands [command :description])
          (get-in commands [command :details])
          ""
          "Arguments:"
          arguments-summary
          ""
          "Specific options:"
          specific-options-summary
          ""
          "Gloabl options:"
          global-options-summary
          ""]
         (s/join \newline))))

(defn validate-arguments
  "Validate arguments only, imitating the way parse-opts does it for options."
  [command arguments options commands global-options-summary]
  (if (:help options)
    {:exit-message (get-command-usage command commands global-options-summary)
     :ok?          true}
    (if-let [arity-error (validate-arity command arguments commands)]
      {:exit-message (error-msg :arity [arity-error])}
      
      (let [args         (->> arguments
                              (map-indexed
                               (fn [i a]
                                 (let [arg-name (get-in commands [command :arguments i 0])]
                                   (when-not (nil? arg-name)
                                     [(str "--" arg-name) a]))))
                              (apply concat))
            args-options (normalize-commands commands)
            opts         (get-in args-options [command :arguments] [])
            arg-opts     (parse-opts args opts)]
        (if-let [errors (:errors arg-opts)]
          {:exit-message (error-msg :arguments errors)}
          {:command   command
           :arguments (:options arg-opts) ; we passed arguments spec as opts
           :options   options})))))

(defn get-commands-summary
  "Builds the usage summary for all the commands."
  [commands]
  (let [spacer       "  "
        with-headers (->> commands
                          (map (fn [[command {:keys [arguments] :as values}]]
                                 [command
                                  (assoc values
                                         :header
                                         (str spacer command " "
                                              (s/join " "
                                                      (map
                                                       (fn [st]
                                                         (s/upper-case (first st)))
                                                       arguments))))]))
                          (into {}))
        mx           (apply max
                            (map (comp count :header val) with-headers))]
    (->> with-headers
         (mapcat (fn [[_ {:keys [header description details]}]]
                   [(str header
                         (apply str (repeat (- mx (count header)) " "))
                         spacer
                         description)
                    (str (apply str (repeat (+ mx (count spacer)) " "))
                         details)
                    ""]))
         (s/join \newline))))
