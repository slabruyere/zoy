(ns zoy.utils.size)

(def ^:private kB 1024)

(def ^:private MB (* kB kB))

(def ^:private GB (* kB MB))

(defn get-reclaimed-space
  [rows]
  (if (> (count rows) 0)
    (let [total-size (->> (map :size rows)
                          (reduce (fnil + 0 0)))]
      (if (> total-size GB)
        (str (float (/ total-size GB)) "GB")
        (if (> total-size MB)
          (str (float (/ total-size MB)) "MB")
          (if (> total-size kB)
            (str (float (/ total-size kB)) "kB")
            (str total-size "B")))))
    "0B"))
