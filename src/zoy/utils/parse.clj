(ns zoy.utils.parse)

(defn parse-int
  [i]
  (try
    (Integer/parseInt i)
    (catch Exception e
      (println (.getMessage e)))))
