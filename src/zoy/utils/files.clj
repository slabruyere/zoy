(ns zoy.utils.files
  (:require [clojure.string :as s]
            [me.raynes.fs :as fs]
            [pandect.algo.sha1 :refer [sha1-file]]))

(defn ^:private get-find-pattern
  [filetypes]
  (re-pattern
   (str "(?i).*\\.("
        (s/join "|" filetypes)
        ")")))

(defn find-files
  [src filetypes]
  (->> filetypes
       get-find-pattern
       (fs/find-files src)
       (filter fs/file?)))

(def ^:private file-hash-location-map
  (atom {}))

(defn reset-file-hash-location-map
  "Used for tests only"
  []
  (reset! file-hash-location-map {}))

(defn ^:private location-taken?
  [location]
  (or (fs/file? location)
      ((into #{} (vals @file-hash-location-map)) location)))

(defn get-available-location
  [{:keys [absolute_path hash]} parent filename extension]
  (loop [index 0]
    (let [location (str parent "/" filename
                        (when (> index 0)
                          (str "(" index ")"))
                        extension)]
      (if (and (not= absolute_path location)
               (location-taken? location))
        (recur (inc index))
        (do (swap! file-hash-location-map assoc hash location)
            location)))))

(defn make-absolute-path
  [path]
  (.getAbsolutePath ^java.io.File (fs/absolute path)))

(defn is-duplicate?
  [{:keys [hash]} parent name extension]
  (if-let [duplicate-of (get @file-hash-location-map hash)]
    duplicate-of
    (let [files-re (re-pattern (str name ".*\\" extension))
          files    (->> (fs/find-files parent files-re)
                        (map (fn [f]
                               {(sha1-file f) [(.getAbsolutePath ^java.io.File f)]}))
                        (apply merge-with concat)
                        (map (juxt key (comp first val)))
                        (into {}))]
      (get files hash))))
