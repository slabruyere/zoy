(ns zoy.utils.operations)

(defn create-mv-operation
  [row]
  {:action :mv
   :args   [row]})

(defn create-rm-operation
  [row]
  {:action :rm
   :args   [row]})
