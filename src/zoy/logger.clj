(ns zoy.logger
  (:require [clj-time.core :as t]
            [clj-time.format :as f]))

(defn ^:private noop [& _])

(defn ^:private prepare-log
  "Takes a level and a message and returns a log with the current date."
  [level message]
  (str
   (f/unparse (f/formatter-local "yyyy-MM-dd HH:mm:ss")
              (t/to-time-zone (t/now) (t/default-time-zone)))
   " [" level "] "
   message))

(defn ^:private cli-logger
  "Takes the authorized levels and returns a function that will print the logs if required."
  [authorized-levels]
  (fn [level prepared-log]
    (when (authorized-levels level)
      (println prepared-log))))

(defn ^:private out-logger
  "Takes the authorized levels and a file to append the logs into.
  Returns a function that will append the prepared log to the log file."
  [authorized-levels out]
  (fn [level prepared-log]
    (when (authorized-levels level)
      (spit out prepared-log :append true))))


(defn get-logger
  "Takes the parsed cli options.
  Returns the function to be uesd for *logger* binding"
  [{:keys [verbose debug out]}]
  (let [cli-authorized-levels (cond-> #{"error"}
                                verbose (conj "info" "warn")
                                debug   (conj "debug"))
        out-authorized-levels (cond-> #{"info" "warn" "error"}
                                debug (conj "debug"))
        loggers               (cond-> []
                                (or verbose
                                    debug) (conj (cli-logger cli-authorized-levels))
                                out        (conj (out-logger out-authorized-levels out)))]
    (if (> (count loggers) 0)
      (fn [level message]
        (let [prepared-log (prepare-log level message)]
          (doseq [logger loggers]
            (logger level prepared-log))))
      noop)))
