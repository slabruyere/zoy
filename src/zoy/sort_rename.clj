(ns zoy.sort-rename
  (:require [clj-time.core :as t]
            [clj-time.format :as f]
            [clojure.string :as s]
            [zoy.utils.files :refer [get-available-location
                                     is-duplicate?]]
            [zoy.utils.information :refer [get-camera-name
                                           get-full-information]]
            [zoy.utils.operations :refer [create-mv-operation
                                          create-rm-operation]]))

(def ^:private months
  {1  "01 January"
   2  "02 February"
   3  "03 March"
   4  "04 April"
   5  "05 May"
   6  "06 June"
   7  "07 July"
   8  "08 August"
   9  "09 September"
   10 "10 October"
   11 "11 November"
   12 "12 December"})

(defn ^:private hydrate-row
  "Associate appropriate year and month to a row.
  If a creation date could not be find in metadata, associates nil for both."
  [{date :date :as row}]
  (if date
    (assoc row
           :year (t/year date)
           :month (get months (t/month date)))
    row))

(defn ^:private fill-move-to
  "Associate appropriate move_to to a row.
  If the deduplicate option is set, this can associate duplicate_of for the file to be deleted."
  [dest {:keys [camera-name deduplicate selection unknown]}
   {:keys [year month favourite date make model extension parent_folder]
    :as   row}]
  (let [year-month     (if (and year month)
                         (str year "/" month)
                         unknown)
        parent         (if dest
                         (->> [dest year-month (when favourite selection)]
                              (remove nil?)
                              (s/join "/"))
                         parent_folder)
        camera         (or camera-name (get-camera-name make model) unknown)
        formatted-date (if date
                         (f/unparse (f/formatter "YYYY-MM-dd HH:mm:ss") date)
                         unknown)
        filename       (str camera " - " formatted-date)
        duplicate-of   (when deduplicate
                         (is-duplicate? row parent filename extension))]
    (if (and dest deduplicate duplicate-of) ; dest is only passed for sort command
      (assoc row :duplicate_of duplicate-of)
      (assoc row :move_to (get-available-location row parent filename extension)))))

(defn create-folders-and-move
  "Returns operations for mkdir and mv."
  [dest {:keys [unknown selection]} [[year month favourite] rows]]
  (let [year-month   (if (and year month)
                       (str year "/" month)
                       unknown)
        dirs         (->> [dest year-month (when favourite selection)]
                          (remove nil?)
                          (s/join "/"))
        rows-to-move (remove #(= (:absolute_path %)
                                 (:move_to %))
                             rows)]
    (when-not (empty? rows-to-move)
      (concat [{:action :mkdir
                :args   [dirs]}
               {:action :log
                :args   ["info" (str "Moving " (count rows-to-move) " files")]}]
              (doall (map create-mv-operation rows-to-move))))))

(defn remove-duplicate
  "Returns rm operation for duplicate file as well as a debug log."
  [{:keys [duplicate_of] :as row}]
  [{:action :log
    :args   ["debug" (str "Removing duplicate of " duplicate_of)]}
   (create-rm-operation row)])

(defn do-rename
  "Return the rename operations."
  [_ options logger files]
  (try
    (logger "info" (str "Found " (count files) " files."))
    (let [to-rename (->> files
                         (map get-full-information)
                         (map (partial fill-move-to nil options))
                         (remove #(= (:absolute_path %)
                                     (:move_to %))))]
      (concat [{:action :log
                :args   ["info" (str "Renaming " (count to-rename) " files.")]}]
              (map create-mv-operation to-rename)))
    (catch Exception e
      (logger "error" (.getMessage e)))))

(defn do-sort
  "Return the sort operations."
  [{dest :dest} {:keys [deduplicate as-selection] :as options} logger files]
  (try
    (logger "info" (str "Found " (count files) " files."))
    (let [rows           (cond->> (map get-full-information files)
                           true         (map hydrate-row)
                           as-selection (map #(assoc % :favourite true))
                           true         (map (partial fill-move-to dest options)))
          to-be-sorted   (cond->> rows
                           deduplicate (remove :duplicate_of)
                           true        (group-by (juxt :year :month :favourite)))
          to-sort        (mapcat
                          (partial create-folders-and-move dest options)
                          to-be-sorted)
          to-be-deleted  (when deduplicate
                           (filter :duplicate_of rows))
          to-deduplicate (when deduplicate
                           (->> to-be-deleted
                                (mapcat remove-duplicate)
                                (cons {:action :log
                                       :args   ["info" (str "Deleting " (count to-be-deleted) " duplicate files.")]})))]
      (concat to-sort to-deduplicate))
    (catch Exception e
      (logger "error" (.printStackTrace e)))))
