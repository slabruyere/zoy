(ns zoy.operations
  (:require [me.raynes.fs :as fs]
            [zoy.utils.files :refer [get-available-location]]))

(def ^:dynamic *options*)

(defn ^:dynamic *logger* [& _])

(defn mv
  "Actually move a file."
  [{:keys [absolute_path move_to]}]
  (*logger* "debug" (str "mv " absolute_path  " " move_to))
  (when-not (:dry-run *options*)
    (fs/rename absolute_path move_to)))

(defn rm
  "Actually delete a file (or move to --rubbish-bin folder)"
  [{:keys [absolute_path] :as row}]
  (if-not (:rubbish-bin *options*)
    (do (*logger* "debug" (str "rm " absolute_path))
        (when-not (:dry-run *options*)
          (fs/delete absolute_path)))
    (let [dest      (:rubbish-bin *options*)
          filename  (fs/name absolute_path)
          extension (fs/extension absolute_path)
          move-to   (get-available-location row dest filename extension)]
      (mv (assoc row :move_to move-to)))))

(defn mkdir
  "Actually create directories needed for path to exist (equivalent to 'mkdir -p')"
  [path]
  (*logger* "info" (str "Creating " path))
  (when-not (:dry-run *options*)
    (fs/mkdirs path)))

(defn perform-operation
  "Dispatch operation to the actual operator."
  [options logger {:keys [action args] :as operation}]
  (binding [*options* options
            *logger*  logger]
    (case action
      :mv                 (apply mv args)
      :rm                 (apply rm args)
      :mkdir              (apply mkdir args)
      :log                (apply logger args)
      (println operation) ;; for debug purposes
      )))
