# zoy

A cli to clean up your digital mess.

## Rationale

We all know someone (aheum...) crawling under backup hard-drives and multiples copies of the same files synced in the cloud. Cleaning up this mess can be exhausting.
The good news is `zoy` is here to help!

## Features

Three actions are currently provided by `zoy`:
- `deduplicate`: checks hashes of files of desired type in given source directory and keeps only one copy.
- `rename`: uses file metadata for pictures and videos in given source directory and renames them accordingly (pattern: `camera - year-month-day hour:minute:seconds.extension`)
- `sort`: renames the files from source directory and moves them to the destination directory in the `year/month` folder.

See [Usage](https://gitlab.com/slabruyere/zoy#usage) for more information on possible options.

## Installation

`zoy` only works on Linux and MacOS at the moment.

### Via the installer script

You need to have Java installed.

```shellsession
$ bash <(curl -s https://gitlab.com/slabruyere/zoy/-/raw/master/scripts/install.sh)
```

By default this will install into /usr/local/bin. To change this, provide the directory name:

```shellsession
$ bash <(curl -s https://gitlab.com/slabruyere/zoy/-/raw/master/scripts/install.sh) /somewhere/in/your/PATH
```

### From the sources

Once you have downloaded the sources, navigate to the `zoy` folder:

```shellsession
$ lein uberjar
$ cat scripts/stub.sh target/uberjar/zoy-0.1.0-SNAPSHOT-standalone.jar > zoy && chmod +x zoy
$ chmod +x zoy
$ mv zoy /somewhere/in/your/PATH
```

## Usage

```shellsession
$ zoy --help

Usage: zoy COMMAND ARGUMENTS [OPTIONS]

A cli to clean up your digital mess.

Gloabl options:
      --confirm                                             Run a dry-run, then ask for user confirmation to perform operations.
      --debug                                               Show debug logs.
      --dry-run                                             Perform a command dry run.
  -h, --help                                                Print this help text.
  -o, --out FILENAME                                        Filename for logs to be written in.
      --partition PARTITION                                 Partition files to handle into lists of size given by this option.
  -t, --types TYPES          ["jp(e?)g" "png" "mp4" "mov"]  Comma-separated file types.
  -v, --verbose                                             Print logs.
      --version                                             Print the current version number of zoy.

Commands:

  deduplicate SRC  Delete duplicate files, using sha1 hash comparison.
                   This command checks the hashes of files of desired type in the given source directory and keeps only one copy.

  rename SRC       Rename files but keep them in the same folder.
                   This command uses file metadata for pictures and videos in given source directory and renames them accordingly (pattern: `camera - year-month-day hour:minute:seconds.extension`).

  sort SRC DEST    Rename and move files in organized folders.
                   This command renames the files from source directory and moves them to the destination directory in the `year/month` folder.


Type "zoy COMMAND --help" for more information on a specific command.

```

### `deduplicate`
```shellsession
$ zoy deduplicate --help

Usage: zoy deduplicate SRC [OPTIONS]

Delete duplicate files, using sha1 hash comparison.
This command checks the hashes of files of desired type in the given source directory and keeps only one copy.

Arguments:
  SRC  Source folder for deduplication.

Specific options:
      --keep-one                            Keep only one file when deduplicating.
      --selection SELECTION      Selection  Name of selection folder.
      --rubbish-bin RUBBISH-BIN             Destination for deleted-to-be files.

Gloabl options:
      --confirm                                             Run a dry-run, then ask for user confirmation to perform operations.
      --debug                                               Show debug logs.
      --dry-run                                             Perform a command dry run.
  -h, --help                                                Print this help text.
  -o, --out FILENAME                                        Filename for logs to be written in.
      --partition PARTITION                                 Partition files to handle into lists of size given by this option.
  -t, --types TYPES          ["jp(e?)g" "png" "mp4" "mov"]  Comma-separated file types.
  -v, --verbose                                             Print logs.
      --version                                             Print the current version number of zoy.

```

_Warning_: using the `--partition` option means you are not ensured complete deduplication since duplicates are checked within every batch of files only.

### `rename`
```shellsession
$ zoy rename --help

Usage: zoy rename SRC [OPTIONS]

Rename files but keep them in the same folder.
This command uses file metadata for pictures and videos in given source directory and renames them accordingly (pattern: `camera - year-month-day hour:minute:seconds.extension`).

Arguments:
  SRC  Source folder for renaming.

Specific options:
      --camera-name CAMERA_NAME           Value for camera name.
      --unknown UNKNOWN          Unknown  Value for unknown dates, makes and models.

Gloabl options:
      --confirm                                             Run a dry-run, then ask for user confirmation to perform operations.
      --debug                                               Show debug logs.
      --dry-run                                             Perform a command dry run.
  -h, --help                                                Print this help text.
  -o, --out FILENAME                                        Filename for logs to be written in.
      --partition PARTITION                                 Partition files to handle into lists of size given by this option.
  -t, --types TYPES          ["jp(e?)g" "png" "mp4" "mov"]  Comma-separated file types.
  -v, --verbose                                             Print logs.
      --version                                             Print the current version number of zoy.

```

### `sort`
```shellsession
$ zoy sort --help

Usage: zoy sort SRC DEST [OPTIONS]

Rename and move files in organized folders.
This command renames the files from source directory and moves them to the destination directory in the `year/month` folder.

Arguments:
  SRC   Source folder for sorting.
  DEST  Destination folder for sorted files.

Specific options:
      --as-selection                        Consider all the sorted files as selection.
      --camera-name CAMERA_NAME             Value for camera name.
      --deduplicate                         Delete duplicates too.
      --selection SELECTION      Selection  Name of selection folder.
      --unknown UNKNOWN          Unknown    Value for unknown dates, makes and models.

Gloabl options:
      --confirm                                             Run a dry-run, then ask for user confirmation to perform operations.
      --debug                                               Show debug logs.
      --dry-run                                             Perform a command dry run.
  -h, --help                                                Print this help text.
  -o, --out FILENAME                                        Filename for logs to be written in.
      --partition PARTITION                                 Partition files to handle into lists of size given by this option.
  -t, --types TYPES          ["jp(e?)g" "png" "mp4" "mov"]  Comma-separated file types.
  -v, --verbose                                             Print logs.
      --version                                             Print the current version number of zoy.

```

## Playground

You can play around with the `src`, `my-reports`, `my-trips` and `my-pictures` folders in the `resources` directory. To initialize them and restore them to their initial state once you have played around with `zoy`, just run `./scripts/restore_resources.sh`. Dont' hesitate to test the commands and options like `--dry-run` to get confidence about using `zoy`!

### Deduplicating PDFs in `my-reports` with `keep-one` and `verbose` options
```shellsession
$ tree resources/my-reports 
resources/my-reports
├── a-folder
│   ├── another-folder
│   │   ├── report1-another-copy.pdf
│   │   └── report2-copy.pdf
│   └── report1-copy.pdf
├── an-image.jpg
├── report1.pdf
└── report2.pdf

2 directories, 6 files

$ zoy deduplicate resources/my-reports -t pdf --keep-one -v
2020-05-21 12:42:39 [info] Found 5 files.
2020-05-21 12:42:39 [info] Deleting 3 files.
2020-05-21 12:42:39 [info] Total reclaimed space: 17.766602kB
2020-05-21 12:42:39 [info] Ran in 0s

$ tree resources/my-reports
resources/my-reports
├── a-folder
│   └── another-folder
├── an-image.jpg
├── report1.pdf
└── report2.pdf

2 directories, 3 files
```

### Renaming videos in `my-trips` with `verbose` and `confirm` options
```shellsession
$ tree resources/my-trips
resources/my-trips
├── Australia
│   ├── australia.jpg
│   └── australia.mp4
└── France
    ├── france.jpg
    └── france.mov

2 directories, 4 files

$ zoy rename resources/my-trips -t mov,mp4 -v
2020-05-26 17:00:47 [info] Found 2 files.
2020-05-26 17:00:47 [info] Renaming 2 files.
2020-05-26 17:00:47 [info] Ran in 0s

$ tree resources/my-trips
resources/my-trips
├── Australia
│   ├── australia.jpg
│   └── Unknown - 2005-10-28 17:46:46.mp4
└── France
    ├── Apple iPhone 6 - 2019-07-24 08:25:57.mov
    └── france.jpg

2 directories, 4 files
```

### Sorting images
```shellsession
$ tree resources/my-pictures 
resources/my-pictures
├── a-folder
│   ├── picture3.jpg
│   └── picture4.png
├── another-folder
│   ├── picture3-copy.jpg
│   └── picture5.png
├── picture1.jpg
└── picture2.jpg

2 directories, 6 files

$ zoy sort resources/my-pictures resources/my-pictures -t jpg,jpeg --deduplicate

$ tree my-pictures
resources/my-pictures
├── 2001
│   └── 04 April
│       └── SONY DCRPC100 - 2001-04-01 16:29:03.jpg
├── 2003
│   └── 03 March
│       └── FUJIFILM FinePix A303 - 2003-03-04 17:14:51.jpg
├── 2011
│   └── 01 January
│       └── Apple iPhone 4 - 2011-01-13 14:33:39.jpg
├── a-folder
│   └── picture4.png
└── another-folder
    └── picture5.png

8 directories, 5 files
```

## Tips for idempotence

`zoy` allows you to sort your files and especially pictures and videos by enforcing a camera-name or a date. But if the same files get sorted again without these options, they will be moved to a different location.
Since `zoy` relies on metadata like EXIF, the safest bet for you to ensure idempotence (ie. the same result no matter how many times you run `zoy`) is to alter the metadata of the file altogether. A great program to achieve this is [exiftools](https://exiftool.org) ([free software](https://exiftool.org/#license)). It supports a great variety of file types and provides helpers to shift dates.

### Using `exiftools` to edit camera make and model

An example of how to edit the camera make and model for all pictures in a given folder:
```shellsession
$ exiftool -r -overwrite_original -Make="new-make" -Model="new-model" /path/to/folder
```

More information:
- https://exiftool.org/exiftool_pod.html
- https://libre-software.net/edit-metadata-exiftool/

### Using `exiftools` to shift dates

An example of how to shift all the pictures taken by a camera of FUJIFILM make by 13 years, 5 months and 18 hours in a given folder:

```shellsession
$ exiftool -alldates+='13:5:0 18' -if '$make eq "FUJIFILM"' /path/to/folder
```

More information:
- https://exiftool.org/exiftool_pod.html#Special-features
- https://exiftool.org/Shift.pdf

## History
`zoy` is a complete rewrite of [sort-my-pics](https://gitlab.com/slabruyere/sort-my-pics-archived) in Clojure. This program was written in bash and worked only on MacOS.

## License

Copyright © 2020 Stéphane Labruyère

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.

The file `src/zoy/utils/information.clj` contains a slightly modified version of code from [exif-processor](https://github.com/joshuamiller/exif-processor), distributed under the MIT License.
Test images and videos in the `resources` folder come from [metadata-extractor-images](https://github.com/drewnoakes/metadata-extractor-images). The pictures have been resized to 1px for wieght purposes.
